'use strict';
var Q = require('q');

function ThreadsClientFactory(storage, btoa) {
    var JSON = global ? global.JSON : window.JSON;

    var ThreadsClient = function(url) {
        if (url[url.length-1] == "/") {
            url = url.substr(0, url.length-1);
        }
        this._service_url = url;
        this._auth_code = null;
        this._client_name = null;
    };

    // Load tries to restore a previous session from the configured storage
    // It returns true if it succeed, false otherwise.
    ThreadsClient.prototype.Load = function() {
        /*
         * Q.fcall works with both sync (browser localStorage) and promise
         * aware (react AsyncStorage) functions
         */
        var pcode = Q.fcall(function() { return storage.getItem("threads_authcode"); });
        var pclient = Q.fcall(function() { return storage.getItem("threads_clientname"); });
        var me = this;
        return Q.all([pcode, pclient])
            .spread(
                function(code, client_name) {
                    if (!(code && client_name)) {
                        me.Reset();
                        return false;
                    } else {
                        me.AuthCode(code, client_name);
                        return true;
                    }
                },
                function(reason) {
                    console.log("load failed with: " + reason);
                    return false;
                });
    };

    // Save persists the session state to the configured storage.
    ThreadsClient.prototype.Save = function() {
        var pcode, pclient;
        // errors are intentionally not handled and forwarded to the caller.
        if (this.Status() == "connected") {
            var me = this;
            pcode = Q.fcall(function() { return storage.setItem("threads_authcode", me._auth_code); });
            pclient = Q.fcall(function() { return storage.setItem("threads_clientname", me._client_name); });
            return Q.all([pcode, pclient])
                .then(function() {
                    return true;
                });
        }
        else {
            pcode = Q.fcall(function() { return storage.removeItem("threads_authcode"); });
            pclient = Q.fcall(function() { return storage.removeItem("threads_clientname"); });
            return Q.all([pcode, pclient])
                .then(function() {
                    return true;
                });
        }
    };

    // Status returns "connected" if the threads client is authenticated; "unknown"
    // otherwise
    ThreadsClient.prototype.Status = function() {
        return this._auth_code ? "connected" : "unknown";
    };

    // ClientName returns the name this client must use with twilio.
    ThreadsClient.prototype.ClientName = function() {
        return this._client_name;
    };

    // AuthCode instruct this client to use the specified code to identify itself
    // with the threads server.
    //
    // After this call the `Status()` method returns "connected".
    //
    // AuthCode doesn't check the validity of the code
    ThreadsClient.prototype.AuthCode = function(code, client_name) {
        if (typeof code == "undefined") {
            return this._auth_code;
        }
        this._auth_code = code;
        this._client_name = client_name;
    };

    // Reset the client state to not authenticated
    ThreadsClient.prototype.Reset = function() {
        this.AuthCode(null, null);
    };

    // NewSession tries to create a new session for the specified telephone
    // number
    ThreadsClient.prototype.NewSession = function(telephone, session_kind) {
        var payload = {telephone: telephone, kind: session_kind || "web"};
        return this._post("/v1beta/me/sessions/", payload);
    };

    // ValidateSession tries to validate a previously created session
    ThreadsClient.prototype.ValidateSession = function(session_id, code) {
        var payload = {smscode: Number(code)};
        return this._post("/v1beta/me/sessions/" + session_id, payload);
    };

    // Profile return or set the user profile
    ThreadsClient.prototype.Profile = function(data) {
        if (typeof data == "undefined") {
            return this._get("/v1beta/me");
        }
        else {
            return this._put("/v1beta/me", data);
        }
    };

    ThreadsClient.prototype.NewThread = function() {
        return this._post("/v1beta/threads/", {});
    };

    ThreadsClient.prototype.ThreadFeedback = function(thread_id, feedback) {
        return this._put("/v1beta/threads/" + thread_id, feedback);
    };

    ThreadsClient.prototype.ThreadsList = function(thread_id, feedback) {
        return this._get("/v1beta/threads/");
    };

    // _url is a private method that returns the absolute url for the path
    ThreadsClient.prototype._url = function(path) {
        return this._service_url + path;
    };

    /*
     * _request is a private method that wraps an async request made with an
     * XMLHttpRequest and expose a promise that will be fullfilled with the
     * server response or error.
     *
     * To not send a payload pass undefined or call this method with just two
     * arguments.
     */
    ThreadsClient.prototype._request = function(method, path, payload) {
        var deferred = Q.defer();
        var http = new XMLHttpRequest();

        http.open(method, this._url(path), true);
        if (this.AuthCode()) {
            http.setRequestHeader("Authorization", "Basic " + btoa(":" + this.AuthCode()));
        }
        var body = undefined;
        if (typeof payload != "undefined") {
            http.setRequestHeader("Content-type", "application/json");
            body = JSON.stringify(payload);
        }
        http.onreadystatechange = function() {
            if (http.readyState == 4) {
                if (http.status < 400) {
                    var parsed;
                    if (http.responseText.length > 0) {
                        parsed = JSON.parse(http.responseText);
                    } else {
                        parsed = null;
                    }
                    deferred.resolve(parsed);
                } else {
                    var err = {
                        code: http.status,
                        responseText: http.responseText
                    };
                    deferred.reject(err);
                }
            }
        };
        http.send(body);
        return deferred.promise;
    };

    ThreadsClient.prototype._post = function(path, payload) {
        return this._request("POST", path, payload);
    };

    ThreadsClient.prototype._put = function(path, payload) {
        return this._request("PUT", path, payload);
    };

    ThreadsClient.prototype._get = function(path, payload) {
        return this._request("GET", path);
    };

    return ThreadsClient;
}

// Base64 implementation for react native
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}};

module.exports = {
    clientFactory: function(storage, btoa) {
        return ThreadsClientFactory(storage, btoa);
    },
    reactNativeClient: function() {
        console.log("react native client class");
        var React = require('react-native');
        return this.clientFactory(React.AsyncStorage, Base64.encode.bind(Base64));
    },
    browserClient: function() {
        console.log("browser client class");
        return this.clientFactory(window.localStorage, Base64.encode.bind(Base64));
    }
};
