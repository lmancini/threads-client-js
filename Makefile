.PHONY: build

build:
	@mkdir -p dist/react
	@mkdir -p dist/browser
	@cp threads_client.js dist/react/threads_client.js
	@browserify threads_client.js --standalone ThreadsClient --im -o dist/browser/threads_client.js
	@cat dist/browser/threads_client.js | uglifyjs --compress > dist/browser/threads_client.min.js
